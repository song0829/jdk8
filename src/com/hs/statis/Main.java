package com.hs.statis;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {

		final Person p = Person.create(Person::new);
		
		final List<Person> pList = Arrays.asList(p);
		
		pList.forEach(Person::print);
		
	}
	

	public static class Person{
	
		public static Person create( final Supplier< Person > supplier ) {
			return supplier.get();
		} 
		
		public static void print(final Person s ){
			
			System.err.println(s);
		
		}
	
	}

}

