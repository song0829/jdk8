package com.hs.time;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Main {

	public static void main(String[] args) throws InterruptedException {

		LocalDateTime ldt = LocalDateTime.now();
		System.err.println(ldt);
		
		
		LocalDate ld = LocalDate.now();
		System.err.println(ld);
		
		LocalTime lt = LocalTime.now();
		System.err.println(lt);
		
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		System.err.println(ldt.format(dateTimeFormatter));
		
		Year y = Year.now();
		System.err.println(y);
		
		YearMonth  m = YearMonth.now();
		System.err.println(m);
		
		MonthDay md = MonthDay.now();
		System.err.println(md);
		
		DayOfWeek dow = DayOfWeek.of(1);
		System.err.println(dow);
		
		Month mo = Month.of(3);
		System.err.println(mo);
		
		ZonedDateTime zdt = ZonedDateTime.now();
		System.err.println(zdt);
		
		Date d = new Date();
		
		Instant i = d.toInstant();
		System.err.println(i);
		Thread.sleep(100);
		i = d.toInstant();
		System.err.println(i);
		
		Clock clock = Clock.systemUTC();
		System.err.println(clock);
		System.err.println(clock.instant());
		System.err.println(clock.millis());
		
	}

}
