package com.hs.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main1 {

	public static void main(String[] args) {
		
		Predicate<String> p = s -> {
			if(null != s && s.length() > 0 && s.indexOf("hs") > -1){
				return true;
			}	
			return false;
		};

		List<String> list = new ArrayList<String>();
		
		list.add("123aaa");
		list.add("456bbbhs");
		list.add("789ccc");
		
		long i = list.stream().filter(p).count();
		long j = list.stream().filter( ss -> (null != ss && ss.length() > 0 && ss.indexOf("hs") > -1)).count();
		long l = list.stream().filter( ss -> {
			if(null != ss && ss.length() > 0 && ss.indexOf("hs") > -1){
				return true;
			}
			return false;
		}).count();
		System.err.println(i);
		System.err.println(j);
		System.err.println(l);
	}

}
