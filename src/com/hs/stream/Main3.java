package com.hs.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main3 {

	public static void main(String[] args) {

//		int count = java.util.stream.Stream.of(1,2,3,4,5).reduce(
//				2 , (x,y) -> x+y
//		);
//		
//		Optional o = java.util.stream.Stream.of(1,2,3,4,5).reduce(
//				(x,y) -> x+y
//		);
//		
//		System.err.println(count);
//		
//		System.err.println(o.get());
		
		List<SubBean> subBeanList = new ArrayList<SubBean>();
		
		subBeanList.add(new SubBean(1));
		subBeanList.add(new SubBean(2));
		subBeanList.add(new SubBean(3));
		subBeanList.add(new SubBean(4));
		
		List<Bean> beanList = new ArrayList<Bean>();
		
		Bean b1 = new Bean();
		b1.beanList = subBeanList;
		
		Bean b2 = new Bean();
		b2.beanList = subBeanList;
		
		beanList.add(b1);
		beanList.add(b2);
		
		print(beanList);
		
		beanList.stream().forEach(bean -> {
			bean.beanList = bean.beanList.stream().filter(sb -> sb.age > 2).collect(Collectors.toList());
		});
		
		System.err.println("------------------------------");
		
		print(beanList);
		
		System.err.println("------------------------------");
		final List<SubBean> tempBeanList = new ArrayList<SubBean>();
		beanList.stream().forEach(bean -> {
			tempBeanList.addAll(java.util.stream.Stream.of(bean.beanList , tempBeanList).flatMap(ss -> ss.stream()).collect(Collectors.toList()));
		});
		
		print1(tempBeanList);
	}
	
	static void print(List<Bean> beanList){
		
		for(Bean bean : beanList){
			System.err.println(bean.toString());
			for(SubBean subBean : bean.beanList){
				System.err.println(subBean.age);
			}
		}
		
	}
	
	static void print1(List<SubBean> beanList){
		
		for(SubBean subBean : beanList){
			System.err.println(subBean.age);
		}
	}
}

class Bean{
	
	List<SubBean> beanList;
	
}
class SubBean{
	int age;

	public SubBean(int age) {
		super();
		this.age = age;
	}
}