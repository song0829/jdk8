package com.hs.stream;

import java.util.List;
import java.util.stream.Collectors;

public class Main2 {

	public static void main(String[] args) {

		List<String> list = java.util.stream.Stream.of("a","bd","c").collect(Collectors.toList());
		
		System.err.println(list.toString());
		
		list = list.stream().map( s -> s.toUpperCase()).collect(Collectors.toList());
		
		System.err.println(list.toString());
		
		list = list.stream().filter( s -> {
			if(s.indexOf("D") > -1) {
				return true;
			}else{
				return false;
			}
		}).collect(Collectors.toList());
		
		System.err.println(list.toString());
		
		List<String> list1 = java.util.stream.Stream.of("12","2","3").collect(Collectors.toList());
		
		list1 = java.util.stream.Stream.of(list,list1).flatMap( ss -> ss.stream() ).collect(Collectors.toList());
		
		System.err.println(list1.toString());
		
		String min = list1.stream().min( (s1,s2) -> s1.length() - s2.length() ).get();
		
		System.err.println(min);
	}
}
