package com.hs.lambda;

public class Main0 {

	public static void main(String[] args) {

		HsInterface hs = () -> System.err.println("OK1");
		
		hs.run();
		
		HsInterface2 rr = dd -> System.err.println("OK1" + dd);//dd是参数
		
		rr.say(" wa haha");
		
		HsInterface hs1 = () -> {
			System.err.println("OK2");
		};
		
		HsInterface3 hs3 = (String x, String y) -> (x + y);
		HsInterface3 hs4 = (x, y) -> (x + y);
		
		HsInterface4<String> hs6 = (x, y) -> System.err.println("OK1");
		
	}

}
