package com.hs.toU;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;


public class Main0 {

	public static void main(String[] args) {

		List<SubBean> subBeanList = new ArrayList<SubBean>();
		
		subBeanList.add(new SubBean(1));
		subBeanList.add(new SubBean(2));
		subBeanList.add(new SubBean(3));
		subBeanList.add(new SubBean(4));
		
		IntSummaryStatistics stat = subBeanList.stream().mapToInt(subBean -> subBean.age).summaryStatistics();
		
		System.err.println(stat.getMax());
		System.err.println(stat.getMin());
		System.err.println(stat.getSum());
		System.err.println(stat.getAverage());
	}

}
class SubBean{
	int age;

	public SubBean(int age) {
		super();
		this.age = age;
	}
}