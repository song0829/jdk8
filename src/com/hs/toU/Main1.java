package com.hs.toU;

import java.util.Optional;

public class Main1 {

	public static void main(String[] args) {

		Parent pp = new ChildImpl();
		pp.p();
		
		
		Optional<String> op = Optional.empty();
		System.err.println(op.isPresent());
//		System.err.println(op.get());
		
		op = Optional.of("sdf");
		System.err.println(op.isPresent());
		System.err.println(op.get());
		
		op = Optional.ofNullable(null);
		System.err.println(op.isPresent());
//		System.err.println(op.get());
	}
}
