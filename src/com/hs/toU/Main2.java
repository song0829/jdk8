package com.hs.toU;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main2 {

	public static void main(String[] args) {

		Set<Integer> set = new HashSet<Integer>();
		set.add(3);
		set.add(1);
		set.add(2);
		set.add(5);
		set.add(4);
		set.add(6);
		set.add(7);
		set.add(9);
		set.add(8);
//		set.forEach(Main2::print);
//		set.parallelStream().forEach(Main2::print);
//		set.forEach(new Main2()::print1);
		
		
		long ns = System.nanoTime();
		Integer i = Stream.of(set).flatMap(s -> s.stream()).mapToInt(s1 -> s1.intValue()).sum();
		long endns = System.nanoTime() - ns;
		
		System.err.println(i + " : " + endns);
		
		long ns1 = System.nanoTime();
		Integer i1 = Stream.of(set).flatMap(s -> s.parallelStream()).mapToInt(s1 -> s1.intValue()).sum();
		long endns1 = System.nanoTime() - ns1;
		
		System.err.println(i1 + " : " + endns1);

	}

	static void print(Integer s){
		System.err.println(s);
	}
	
	void print1(Integer s){
		System.err.println(s);
	}
}
