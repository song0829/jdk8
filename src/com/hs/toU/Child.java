package com.hs.toU;

public interface Child extends Parent {

	@Override
	default void p() {
		System.err.println("Child.p");
	}
	
}
