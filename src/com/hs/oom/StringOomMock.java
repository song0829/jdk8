package com.hs.oom;

import java.util.ArrayList;
import java.util.List;
 
public class StringOomMock {
	
	//Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
	static String base = "string";
	public static void main(String[] args) {
		
		System.err.println(System.getProperty("java.version"));
		
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			String str = base + base;
			base = str;
			list.add(str.intern());
		}
	}
}
