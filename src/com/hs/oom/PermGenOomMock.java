package com.hs.oom;


import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
 
public class PermGenOomMock {
	
	//Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
	public static void main(String[] args) {
		
		System.err.println(System.getProperty("java.version"));
		
		URL url = null;
		List<ClassLoader> classLoaderList = new ArrayList<ClassLoader>();
		try {
			url = new File("/tmp").toURI().toURL();
			URL[] urls = {url};
			while(true) {
				ClassLoader loader = new URLClassLoader(urls);
				classLoaderList.add(loader);
				loader.loadClass("com.hs.stream.Main3");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}