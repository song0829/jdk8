package com.jds.demo;

import java.util.ArrayList;

public class Generic {

	public static void main(String[] args) {
		
		Class c1 = new ArrayList<String>().getClass();
		Class c2 = new ArrayList<Integer>().getClass();
		
		System.err.println(c1 == c2); //true
		
		Class c3 = new ArrayList<>().getClass();
		Class c4 = new ArrayList<>().getClass();
		
		System.err.println(c3 == c4); //true
		
		Object o = new ArrayList<String>().getClass().getTypeParameters();
		System.err.println(o);
	}
}
