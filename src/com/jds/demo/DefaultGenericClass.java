package com.jds.demo;

public class DefaultGenericClass<E> extends AbstractGenericClass<E>{

	@Override
	public String generic(E e) {
		return e.toString();
	}

	
	public static void main(String[] args) {
		
		DefaultGenericClass<Param> dgc = new DefaultGenericClass<Param>();
		System.err.println(dgc.generic(new Param()));
	}
}
