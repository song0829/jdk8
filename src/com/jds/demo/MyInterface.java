package com.jds.demo;

public interface MyInterface {

	public String name = "d";
	
	public void comMethod();
	
	public abstract void abstractMethod();
	
	default void defaultMethod() {
		System.out.println("DefalutTest defalut 方法");
	}

	static void staticMethod() {
		System.out.println("DefalutTest static 方法");
	}

}
