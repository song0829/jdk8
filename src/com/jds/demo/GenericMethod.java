package com.jds.demo;

import java.util.ArrayList;
import java.util.List;

public class GenericMethod {

	public static <E> void print(List<E> list){
		
		for(E e : list){
			System.err.println(e.toString());
		}
		
	}
	
	
	public static void main(String[] args) {
		
		List<String> list = new ArrayList<String>();
		list.add("111");
		list.add("222");
		list.add("333");
		
		print(list);
	}
}
