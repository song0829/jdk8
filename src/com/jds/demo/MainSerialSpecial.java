package com.jds.demo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainSerialSpecial {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		SerialSpecial ss = new SerialSpecial();
		ss.firstName = "han";
		ss.lastName = "song";
		ss.address = "北京";

		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("result.obj"));
        out.writeObject(ss);
        out.close();
	}
}
