package com.jds.demo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainSerial2 {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		SerialTest st = new SerialTest();
		st.name = "dbhansong ";
		st.address = "北京";
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream op = new ObjectOutputStream(baos);
        op.writeObject(st);
        byte[] bytes = baos.toByteArray();
		
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream( bais);
        
        SerialTest newSt = (SerialTest)ois.readObject();
        
        System.err.println(newSt.name); //dbhansong
        System.err.println(newSt.address); //北京
	}
	
}
