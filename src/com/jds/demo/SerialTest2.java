package com.jds.demo;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class SerialTest2 implements Externalizable {

	String name;
	String address;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(this.name.getBytes());
		out.writeObject(this.address.getBytes());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.name = new String((byte[])in.readObject());
		this.address = new String((byte[])in.readObject());
	}
}
