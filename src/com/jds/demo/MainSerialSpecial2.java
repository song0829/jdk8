package com.jds.demo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainSerialSpecial2 {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

        ObjectInputStream oin = new ObjectInputStream(new FileInputStream("result.obj"));
        SerialSpecial newSs = (SerialSpecial) oin.readObject();
        oin.close();
        
        System.err.println(newSs.firstName); //han
        System.err.println(newSs.lastName); //null
        System.err.println(newSs.address); //null
	}

}
