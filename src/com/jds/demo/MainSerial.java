package com.jds.demo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainSerial {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		SerialTest2 st2 = new SerialTest2();
		st2.name = "dbhansong ";
		st2.address = "北京";
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream op = new ObjectOutputStream(baos);
        op.writeObject(st2);
        byte[] bytes = baos.toByteArray();
		
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream( bais);
        
        SerialTest2 newSt2 = (SerialTest2)ois.readObject();
        
        System.err.println(newSt2.name); //dbhansong
        System.err.println(newSt2.address); //北京
	}
	
}
