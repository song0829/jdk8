package com.jds.demo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainFatherSun {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		SunSerial sun = new SunSerial();
		sun.sunName = "张三";
		
		FatherSerial father = new FatherSerial();
		father.fatherName = "张一 ";		
		father.messages = new String[]{"信息1","信息2"};
		
		sun.fatherSerial = father;
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream op = new ObjectOutputStream(baos);
        op.writeObject(sun);
        byte[] bytes = baos.toByteArray();
		
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream( bais);
        
        SunSerial newSun = (SunSerial)ois.readObject();
        
        System.err.println(newSun.fatherSerial.fatherName); //张一 
        System.err.println(newSun.sunName); //张三
        System.err.println(newSun.fatherSerial.messages[0]); //信息1
	}
}
