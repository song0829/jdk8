package com.jvm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 
 *  @Description 
 *  
 *  -Xms256m -Xmx256m -XX:+UnlockExperimentalVMOptions -Xss256k -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:CMSFullGCsBeforeCompaction=0 -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 -XX:+CMSClassUnloadingEnabled -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=52001 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.awt.headless=true -Dsun.net.client.defaultConnectTimeout=60000 -Dsun.net.client.defaultReadTimeout=60000 -Djmagick.systemclassloader=no -Dnetworkaddress.cache.ttl=300 -Dsun.net.inetaddr.ttl=300 -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/export/Instances/serial_log/server1/logs -XX:ErrorFile=/export/Instances/serial_log/server1/logs/java_error_%p.log
 *  
 *
 *  @author  hansong
 *  @date  2020年6月26日 下午3:03:06
 */
public class MainJvm {
	
	static Map<String , TempBean> map = new HashMap<String , TempBean>();
	
	public static void main(String[] args) throws InterruptedException {
		
		Thread.sleep(20000);
		
		System.err.println("START");
		
		for(int i = 0 ; i < 10000000 ; i++){
			
			TempBean bean = getBean(i);
			
			new TempThread(bean).start();
			
			map.put(String.valueOf(i), bean);
			
//			Thread.sleep(200);
		}
		System.err.println("END");
		
		Thread.sleep(100000000);
	}
	
	static TempBean getBean(int i){
		
		String name = "测试jdk";
		Integer age = 99;
		String address = "北京北京市大兴区高新技术园区,经海路和科创十一街交汇,北京汀塘3号楼";
		String info = "我就是要测试内存情况，看看jdk8的内存到底啥样子";
		return new TempBean(name+i, age+i, address+i, info+i);
	}
	
}

class TempThread extends Thread{
	
	TempBean tempBean;
	
	public TempThread(TempBean tempBean) {
		super();
		this.tempBean = tempBean;
	}
	
	List<TempBean> list = new ArrayList<TempBean>();

	@Override
	public void run() {
		for(int i = 0 ; i < 100 ; i ++){
			list.add(new TempBean(tempBean.getName()+i,tempBean.getAge()+i,tempBean.getAddress()+i,tempBean.getInfo()+i));
		}
	}
}

class TempBean{
	
	public TempBean(String name, Integer age, String address, String info) {
		super();
		this.name = name;
		this.age = age;
		this.address = address;
		this.info = info;
	}

	private String name;
	
	private Integer age;
	
	private String address;
	
	private String info;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
}
